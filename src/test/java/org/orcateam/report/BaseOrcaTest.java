package org.orcateam.report;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;

import java.io.*;

public abstract class BaseOrcaTest {

    private String testPath = null;

    @Before
    public void init() {
        testPath = System.getProperty("java.io.tmpdir") + "/rendered-reports/";
        File path = new File(testPath);
        path.mkdir();
        Assert.assertFalse(StringUtils.isBlank(testPath));
    }

    public String writeTestResults(byte[] output, String ext) {
        String fileName = getTestPath() + DigestUtils.md5Hex(new Long(System.currentTimeMillis()).toString()) + ext;
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(new File(fileName));
            outputStream.write(output);
            outputStream.flush();
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        return fileName;
    }

    public String getTestPath() {
        return testPath;
    }

}
