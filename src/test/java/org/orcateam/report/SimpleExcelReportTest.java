package org.orcateam.report;

import org.junit.Test;
import org.orcateam.report.engine.xlsx.ExcelXReport;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SimpleExcelReportTest extends BaseOrcaTest {


    @Test
    public void test() throws Exception {
        InputStream is = getClass().getResourceAsStream("/simpleExcelReport.xlsx");
        OrcaReport report = new ExcelXReport(is);
        report.put("hello", "Hello World!");
        report.put("itemList", dataList());
        byte[] output = report.render();

        writeTestResults(output, ".xlsx");

    }


    private List<People> dataList() {
        List<People> data = new ArrayList<People>();
        data.add(new People(1, "Kamil ORS", "kamilors@gmail.com", "1111", "ISTANBUL"));
        data.add(new People(1, "Esra Ors", "kamilors@gmail.com2", "11112", "ISTANBUL2"));
        data.add(new People(1, "M. Ali Ors", "kamilors@gmail.com3", "11113", "ISTANBUL3"));
        data.add(new People(1, "Nursel Ors", "kamilors@gmail.com4", "11114", "ISTANBUL4"));
        return data;
    }

    public class People {

        private Integer id;
        private String name;
        private String email;
        private String phone;
        private String address;

        public People(Integer id, String name, String email, String phone, String address) {
            this.id = id;
            this.name = name;
            this.email = email;
            this.phone = phone;
            this.address = address;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }
}
