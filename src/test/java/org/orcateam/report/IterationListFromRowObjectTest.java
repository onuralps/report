package org.orcateam.report;

import org.junit.Test;
import org.orcateam.report.engine.docx.DocXReport;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class IterationListFromRowObjectTest extends BaseOrcaTest {

    @Test
    public void test() throws Exception {

        InputStream inputStream = getClass().getResourceAsStream("/IterationListFromRowObject.docx");
        OrcaReport orcaReport = new DocXReport(inputStream);

        // table:source
        List<People> peopleList = generatePeopleList(6, 40);

        orcaReport.put(OrcaReportParam.NULL_STRING, "...");

        // we generate the form which will include the people list.
        // views are often used to reduce the input count that should be passed to the orca report
        // and also a good way to organize form values.
        FormView view = new FormView();
        view.setPeeps(peopleList);

        orcaReport.put("view", view);

        byte[] output = orcaReport.render();
        writeTestResults(output, ".docx");

    }

    private List<People> generateRelatives(Integer peopleCountModifier, Integer ageModifier, Integer personIndex) {
        List<People> peopleList = new ArrayList<People>();
        for (int i = 1; i < new Random().nextInt(3) + peopleCountModifier; i++) {
            People people = new People("Relative " + personIndex + "." + i, new Random().nextInt(5) + ageModifier);
            peopleList.add(people);
        }
        return peopleList;
    }

    private List<People> generatePeopleList(Integer peopleCountModifier, Integer ageModifier) {
        List<People> peopleList = new ArrayList<People>();
        for (int i = 1; i < peopleCountModifier; i++) {
            People people = new People("Person " + i, new Random().nextInt(5) + ageModifier);
            people.setRelatives(generateRelatives(3, 20, i));
            peopleList.add(people);
        }
        return peopleList;
    }

    public class People {

        private String name;
        private Integer age;
        private List<People> relatives = new ArrayList<People>(0);

        public People(String name, Integer age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public List<People> getRelatives() {
            return relatives;
        }

        public void setRelatives(List<People> relatives) {
            this.relatives = relatives;
        }
    }

    public class FormView {
        private List<People> peeps;

        public List<People> getPeeps() {
            return peeps;
        }

        public void setPeeps(List<People> peeps) {
            this.peeps = peeps;
        }
    }

}
