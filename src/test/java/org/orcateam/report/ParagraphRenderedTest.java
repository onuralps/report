package org.orcateam.report;

import org.junit.Test;
import org.orcateam.report.engine.docx.DocXReport;

import java.io.InputStream;
import java.util.*;

public class ParagraphRenderedTest extends BaseOrcaTest {
    @Test
    public void test() throws Exception {
        InputStream inputStream = getClass().getResourceAsStream("/ParagraphRendered.docx");
        OrcaReport orcaReport = new DocXReport(inputStream);

        orcaReport.put(OrcaReportParam.NULL_STRING, "...");
        orcaReport.put("nullExpression", null);
        orcaReport.put("notNullExpression", "not null");
        orcaReport.put("renderedCell", null);

        // table:source
        List<People> peopleList = new ArrayList<People>();
        for (int i = 1; i < new Random().nextInt(3) + 3; i++) {
            People people = new People("Person " + i, new Random().nextInt(5) + 20);
            people.getPuans().put(2013, new Random().nextInt(100));
            people.getPuans().put(2014, new Random().nextInt(100));
            people.getPuans().put(2015, new Random().nextInt(100));
            people.getPuans().put(2016, new Random().nextInt(100));
            people.getPuans().put(2017, new Random().nextInt(100));
            // Lets skip this year
            // people.getPuans().put(2018, new Random().nextInt(100));
            people.getPuans().put(2019, new Random().nextInt(100));
            people.getPuans().put(2020, new Random().nextInt(100));

            peopleList.add(people);
        }

        //column:source
        List<Integer> yearList = new ArrayList<Integer>();
        yearList.add(2013);
        yearList.add(2014);

        orcaReport.put(OrcaReportParam.NULL_STRING, "...");
        orcaReport.put("peopleList", peopleList);
        orcaReport.put("yearList", yearList);

        try {
            byte[] output = orcaReport.render();
            writeTestResults(output, ".docx");
            //String results = writeTestResults(output);
            //c(results);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class People {

        private String name;
        private Integer age;
        private Map<Integer, Integer> puans = new HashMap<Integer, Integer>(0);

        public People(String name, Integer age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public Map<Integer, Integer> getPuans() {
            return puans;
        }

        public void setPuans(Map<Integer, Integer> puans) {
            this.puans = puans;
        }
    }
}
