package org.orcateam.report.engine.docx;

import java.io.InputStream;

public class ImageData {
    public String elName;
    public InputStream image;
    public int h;
    public int w;
}
