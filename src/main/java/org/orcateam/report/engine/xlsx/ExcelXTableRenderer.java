package org.orcateam.report.engine.xlsx;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.orcateam.report.OrcaReport;
import org.orcateam.report.exception.OrcaReportRendererException;
import org.orcateam.report.renderer.TableRenderer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExcelXTableRenderer extends TableRenderer {

    XSSFSheet sheet;

    public ExcelXTableRenderer(Object metaCell, XSSFSheet sheet, OrcaReport orcaReport, String metaSring, Map<String, Object> context) throws Exception {
        super(metaCell, orcaReport, metaSring, context);
        this.sheet = sheet;
        initMetadata();
    }

    @Override
    protected Integer getFirstColIndex() {
        Integer metaRowIndex = getMetaCell().getRowIndex();
        Row firstRow = sheet.getRow(metaRowIndex + 1);
        return firstRow.getCell(getMetaCell().getColumnIndex()).getColumnIndex();
    }

    @Override
    protected Integer getFirstRowIndex() {
        return getMetaCell().getRowIndex() + 1;
    }

    @Override
    public void render() throws OrcaReportRendererException {
        addDynamicColumns();
        addDynamicRows();
        getMetaCell().setCellValue(getMetadata("title").getValue());
    }

    public int renderAndGetTableRowSize() throws OrcaReportRendererException {
        render();
        return rowDataList.size();
    }


    private void addDynamicColumns() {
        if (columnData == null || columnData.getValue() == null || columnDataList == null) {
            dynamicColumnIndex = -1;
            return;
        }
    }

    private void addDynamicRows() throws OrcaReportRendererException {
        if (rowData == null || rowData.getValue() == null || rowDataList == null) {
            dynamicRowIndex = -1;
            return;
        }

        int regionSize = sheet.getNumMergedRegions();

        int startTableCol = -1, endTableCol = -1;

        for (int r = 0; r < regionSize; r++) {
            CellRangeAddress region = sheet.getMergedRegion(r);
            if (region.getFirstRow() == getMetaCell().getRowIndex()) {
                if (region.getFirstColumn() == getMetaCell().getColumnIndex()) {
                    startTableCol = region.getFirstColumn();
                    endTableCol = region.getLastColumn();
                    break;
                }
            }
        }

        if (startTableCol == -1 || endTableCol == -1) {
            // bizim exception.
        }

        Integer temprowIndex = (getFirstRowIndex() + dynamicRowIndex) - 1;
        Row temprow = sheet.getRow(temprowIndex);

        List<Integer> temrowRegionList = new ArrayList<Integer>();
        for (int r = 1; r < regionSize; r++) {
            CellRangeAddress region = sheet.getMergedRegion(r);
            if (region.getFirstRow() == temprowIndex && region.getLastRow() == temprowIndex) {
                temrowRegionList.add(r);
            }
        }


        int datarowindex = 0, datacolindex = 0;
        for (int i = 1; i <= rowDataList.size(); i++) {
            boolean isMergeCell = false;
            int newrowindex = temprowIndex + i;
            Row newrow = sheet.getRow(newrowindex);
            if (newrow != null && i != rowDataList.size()) {
                sheet.shiftRows(newrowindex - 1, sheet.getLastRowNum(), 1);
                newrow = sheet.createRow(newrowindex - 1);
                isMergeCell = true;
            }
//            else if(newrow == null) {
//                newrow = sheet.createRow(newrowindex - 1);
//            }
            else {
//                newrow = sheet.getRow(temprowIndex + i - 1);
                newrow = sheet.getRow(newrowindex - 1);
            }


            if (isMergeCell) {
                // yeni olusan satirdaki merged celleri olusturur.
                for (Integer r : temrowRegionList) {
                    CellRangeAddress region = sheet.getMergedRegion(r);
                    sheet.addMergedRegion(new CellRangeAddress(newrow.getRowNum(), newrow.getRowNum(), region.getFirstColumn(), region.getLastColumn()));
                }
            }


            for (int colindex = startTableCol; colindex <= endTableCol; colindex++) {
                Cell newcell;
                Cell tempcell = temprow.getCell(colindex);

                if (temprow.getRowNum() == newrow.getRowNum()) {
                    newcell = newrow.getCell(colindex);
                } else {
                    newcell = newrow.createCell(tempcell.getColumnIndex());
                    newcell.setCellStyle(tempcell.getCellStyle());
                    newcell.setCellValue(tempcell.toString());
                }

                Map<String, Object> context = getCellContext(datarowindex, datacolindex);

                ExcelXCellRenderer cellRenderer = new ExcelXCellRenderer(newcell, getOrcaReport(), context);
                cellRenderer.render();
                datacolindex++;
            }
            datarowindex++;
        }

    }

    /**
     * Get context for cell
     *
     * @param rowindex
     * @param colindex
     * @return Map
     */
    public Map<String, Object> getCellContext(Integer rowindex, Integer colindex) {
        Map<String, Object> context = new HashMap<String, Object>();

        if (rowDataList == null && columnDataList == null) {
            return context;
        }

        if (columnDataList != null) {
            if (colindex >= 0 && colindex < columnDataList.size()) {
                context.put("colindex", colindex);
                Object columnVal = columnDataList.get(colindex);
                context.put(columnData.getVar(), columnVal);
            }
        }

        if (rowDataList != null) {
            if (rowindex >= 0 && rowindex < rowDataList.size()) {
                Object object = rowDataList.get(rowindex);
                context.put(rowData.getVar(), object);
                context.put("rowindex", rowindex);
            }
        }
        return context;
    }

    public Cell getMetaCell() {
        return (Cell) getBody();
    }

}
